---
title: "Web Deployment From Scratch"
logoImg: "assets/logo.png"
theme : "solarized"
highlightTheme: "solarized-light"
transition: "slide"
slideNumber: false
controls: false
progress: false
enableMenu: false
enableChalkboard: false
enableTitleFooter: false
enableSearch: false
---

# Web Deployment From Scratch

Or a copypaste showcase from Digital Ocean tutorials :'V

<small>[orlando@4plus1creative.com](orlando@4plus1creative.com)</small>

---

## Introduction

![howdy](assets/howdy.png)

--

## Why is it important?

![thonks](assets/thonks.jpg)

---

## Schedule

1. Getting a domain {.fragment}

2. Getting a server {.fragment}

3. Configuring the server  {.fragment}

4. Puting a project in the server {.fragment}

5. Pointing the domain to the server {.fragment}

?. Debugging the nginx config file again {.fragment}

---

## (Main) Tecnologies

- Digital Ocean (Infrastructure provider)
- CentOS (Linux distro)
- nginx (Web server)
- git (lorem ipsum)
- certbot (Autotool for SSL/TLS certs)

---

## Getting a domain

#### Top level domains

![TDL](assets/tld.jpg)

--

## Getting a domain

#### Domain name registrars

![Registrars](assets/registrars.jpg)

--

## Getting a domain

#### Personal recommendation

![namecheap](assets/namecheap.jpg)

--

## Getting a domain

#### pls avoid

![EIG](assets/eig.png)

--

## Getting a domain

#### [Insert demo here]

---

## Getting a server

#### Cloud Infrastructure Providers

![Providers](assets/providers.jpg)

--

## Getting a server

#### Dat *aaS

![aaS](assets/aas.jpg)

--

## Getting a server

#### Personal Recommendation

![Digital Ocean](assets/do.png)

--

## Getting a server

#### [Insert demo here]

https://m.do.co/c/a8b08a993bbb

---

## Configuring the server

--

## Configuring the server

#### This is going to take a while

![Waiting](assets/waiting.png)

--

## Configuring the server

#### TAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKATAKA

![Typing](assets/typing.gif)

--

## Configuring the server

![Typing](assets/stop.gif)

--

## Configuring the server

#### Step by step

1. Who, what, where? {.fragment}
2. Enabling extra packages {.fragment}
3. Installing basic packages {.fragment}
4. Enabling and starting services {.fragment}
5. Adding an entry in the firewall {.fragment}
6. Getting packages for the project {.fragment}
7. Let's restart the system {.fragment}
8. Puting a project in the server {.fragment}
9. Building the project {.fragment}
10. Using a process manager {.fragment}
11. Configuring the web server {.fragment}

--

## Configuring the server

#### Let's login first...

![*sweats profusely*](assets/sweats.png)

--

## Configuring the server

#### Who, where, what?

- ` whoami`
- ` pwd`
- ` cat /etc/*-release`

--

## Configuring the server

#### Enabling extra packages

- ` yum install -y epel-release`

<small class="fragment">https://fedoraproject.org/wiki/Yum</small>

<small class="fragment">https://fedoraproject.org/wiki/EPEL</small>

--

## Configuring the server

#### Installing basic packages

- ` yum install -y firewalld`
- ` yum install -y nginx`
- ` yum install -y nano htop git`

<small class="fragment">https://firewalld.org/documentation</small>

<small class="fragment">https://nginx.org/en/docs</small>

--

## Configuring the server

#### Enabling and starting services

- ` systemctl enable firewalld`
- ` systemctl start firewalld`

<small class="fragment">https://wiki.archlinux.org/index.php/systemd</small>

--

## Configuring the server

#### Enabling and starting services (for nginx)

- ` systemctl enable nginx`
- ` systemctl start nginx`

--

## Configuring the server

#### Adding an entry in the firewall

- ` firewall-cmd --zone=public \` <br>
`--add-port=80/tcp \`<br>
`--permanent`
- ` firewall-cmd --reload`

<small class="fragment">https://firewalld.org/documentation/man-pages/firewall-cmd.html</small>

--

## Configuring the server

#### Getting packages for the project

- <small>`curl -sL https://rpm.nodesource.com/setup_10.x | bash -`</small>

- ` yum install -y nodejs`

- ` node -v`

<small class="fragment">https://github.com/nodesource/distributions</small>

--

## Configuring the server

#### Let's restart the system

- ` nano /etc/environment`
- ` nano /etc/selinux/config`
- ` reboot now`

<small class="fragment">https://wiki.debian.org/EnvironmentVariables</small>

<small class="fragment">https://wiki.centos.org/HowTos/SELinux</small>

--

## Configuring the server

#### Puting a project in the server

- ` cd $HOME`
- ` mkdir projects`
- ` cd librebikes`

- <small>`git clone https://gitlab.com/septum___/librebikes.git`</small>

--

## Configuring the server

#### Building the project

- ` npm install`
- ` npm run build`

--

## Configuring the server

#### Using a process manager

- `npm install pm2 -g`
- `pm2 --name lbikes start npm -- start`

--

## Configuring the server

#### Configuring the web server

- `nano /etc/nginx/nginx.conf`
- `nginx -t`
- `systemctl restart nginx`

<small class="fragment">https://nginx.org/en/docs/beginners_guide.html</small>

---

## Pointing the domain to the server

![Pointing](assets/pointing.jpg)

--

## Pointing the domain to the server

#### Common Records

- A Record {.fragment}

- CNAME Record {.fragment}

![bestrecord](assets/bestrecord.jpg) {.fragment}

---

## And We Are Done!

http://librebikes.septum.io

--

## And We Are Done!

![aws](assets/aws.png)

---

## Conclusions

#### Final Thoughts

- Make it your own {.fragment}
- Keep learning {.fragment}

--

## Conclusions

#### Q & A

![deepthonks](assets/deepthonks.png)

---

## Thank You!

###### Comment Like and Suscribe
